#!/usr/bin/env python3

# Stdlib

import os
import argparse

# Internal

from pypkg import *


# I hate this, but it works
cfg = config.Config()


if __name__ == '__main__':
    aparse = argparse.ArgumentParser(prog='pkg.py',
                                     description='Personal package manager')
    aparse.add_argument('--version', help='Show version information', action='store_true')
    aparse.add_argument('--config',  help='Specify config file.',     action='store')

    args = aparse.parse_args()

    if args.config and os.path.isfile(args.config):
        cfg = config.Config(args.config)

    for pkg in cfg.packages:
        obj = package.Package(cfg.dirs.packages+'/'+pkg+'.yml', cfg)
        lst = obj.latest()
        for v in lst:
            if not obj.validate(v):
                print(f'\033[1;32m**\033[0m Fetching {pkg}-{v}')
                obj.fetch(v)
                if obj._pkg.extract.enable:
                    print(f'\033[1;32m**\033[0m Extracting {pkg}-{v}')
                    obj.extract(v)
                if obj._pkg.configure.enable:
                    print(f'\033[1;32m**\033[0m Configuring {pkg}-{v}')
                    obj.configure(v)
                if obj._pkg.build.enable:
                    print(f'\033[1;32m**\033[0m Building {pkg}-{v}')
                    obj.build(v)
                if obj._pkg.install.enable:
                    print(f'\033[1;32m**\033[0m Installing {pkg}-{v}')
                    obj.install(v)
            
            
