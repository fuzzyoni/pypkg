
class Edict(dict):

    def __init__(self, data={}):
        dict.__init__(self, data)
        self.__traverse__()

    def __traverse__(self):
        for key in self.keys():
            if type(self.__getitem__(key)) == dict:
                self.__setitem__(key, Edict(self.__getitem__(key)))

    def __getattr__(self, attr):
        return self.__getitem__(attr)

    def __setattr__(self, attr, val):
        return self.__setitem__(attr, val)
