
# Stdlib

import os
import sys

# Internal

from .command import Command

class Extract(Command):

    def __init__(self, tarball=False, ddir=False):
        if False in (tarball, ddir):
            raise(ValueError)
        self.tarball = tarball
        self.src_dir = os.path.dirname(tarball)
        if ddir:
            self.dst_dir = ddir
        else:
            self.dst_dir = self.src_dir
            
        command = ' '.join([
            self._which('tar'),
            '-%svxf' % self._compress_opt(),
            self.tarball,
            '-C %s' % self.dst_dir
        ])
        Command.__init__(self, tag=f'extract-{os.path.basename(tarball).split(".t")[0]}', command=command)
        self.run()


    def _compress_opt(self):
        ext = self.tarball.split('.')[-1]
        if ext in ('gz', 'tgz'):
            return 'z'
        elif ext in ('bz2', 'tbz', 'tbz2'):
            return 'j'
        elif ext in ('xz', 'txz'):
            return 'J'


