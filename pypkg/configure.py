
# Stdlib

import os
import sys

# Internal

from .command import Command

class Configure(Command):

    def __init__(self, args=False, tdir=False):
        fp = open('/tmp/debug-log', 'w+')
        if type(args) not in (str, list, tuple):
            raise(ValueError('Invalid arguments given to Configure().'))

        if type(tdir) != str or not os.path.isdir(tdir):
            raise(ValueError('Invalid target directory given to Configure()'))

        if type(args) == str:
            self._args = args.split()
        elif type(args) in (list, tuple):
            self._args = args

        fp.write(' '.join(args)
        print(tdir)
        cmd = f'./configure {" ".join(args)}'
        print(cmd)
        Command.__init__(self, tag='configure', command=cmd)
        #self.run(tdir)
