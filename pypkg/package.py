
# Stdlib

import os
import re
import urllib.error
import urllib.request
from distutils.version import StrictVersion

# 3rd party

import yaml
from edict             import Edict
from rslurp            import Rslurp

# Internal

from .build            import Build
from .fetch            import fetch
from .extract          import Extract
from .configure        import Configure


class Package(object):

    versions = []
    uris     = Edict()
    
    def __init__(self, pfn=False, cfg=False):
        if not cfg:
            raise(ValueError('Invalid configuration object.'))
        if not pfn or not os.path.isfile(pfn):
            raise(ValueError(f'Invalid package file: {pfn}'))
        self._cfg = cfg
        self._pkg = Edict(yaml.load(open(pfn).read()))
        self._get_versions()
        self.versions.sort(key=StrictVersion)
        self.versions.reverse()
        
    def _parse_version(self, line=False):
        if line and type(line) == str:
            _tl = re.sub('HREF', 'href', line.strip())
            line = _tl
            uri = line.strip().split('href="')[1].split('">')[0]
            _fn = os.path.basename(uri)
            if _fn.find(self._pkg.name+'-') != -1:
                vers = _fn.split(self._pkg.name+'-')[1].split(self._pkg.versions.extension)[0]
            else:
                vers = _fn.split(self._pkg.name)[-1].split(self._pkg.versions.extension)[0]

            if vers not in self.versions:
                self.versions.append(vers)
                if uri == f'{self._pkg.name}-{vers}{self._pkg.versions.extension}':
                    if self._pkg.versions.url[-1] != '/':
                        uri = f'{self._pkg.versions.url}/{self._pkg.name}-{vers}{self._pkg.versions.extension}'
                    else:
                        uri = f'{self._pkg.versions.url}{self._pkg.name}-{vers}{self._pkg.versions.extension}'
                self.uris[vers] = uri
                
    def _get_versions(self):
        rq = urllib.request.Request(self._pkg.versions.url)
        rs = Rslurp(urllib.request.urlopen(rq))
        rs.add_callback(self._pkg.versions.pattern, self._parse_version)
        rs.process()

    def validate(self, ver=False):
        def _exists(v):
            dn = f'{self._cfg.dirs.library}/{self._pkg.name.lower()}-{v}'
            for tst in self._pkg.test.exists:
                if not os.path.exists(f'{dn}/{tst}'):
                    return False
            print(f'{self._pkg.name}-{ver} is installed')
            return True

        if 'test' in self._pkg.keys():
            for test in self._pkg.test.keys():
                if test == 'exists':
                    return _exists(ver)
        else:
            return True

    def extract(self, ver=False):
        o = f'{self._cfg.dirs.temp}/'
        if not ver:
            for v in self.latest():
                f = f'{self._cfg.dirs.temp}/{os.path.basename(self.uris[self.latest()][0])}'
                obj = Extract(f, o)
        else:
            obj = Extract(f'{self._cfg.dirs.temp}/{os.path.basename(self.uris[ver])}', o)
        
    def fetch(self, vers=False):
        if not vers and len(self.latest()) == 1:
            return fetch(self.uris[self.latest()[0]], self._cfg.dirs.temp)
        
        elif not vers and type(self.latest()) in (list, tuple) and len(self.latest()) > 1:
            for tv in self.latest():
                return fetch(self.uris[tv], self._cfg.dirs.temp)

        else:
            return fetch(self.uris[vers], self._cfg.dirs.temp)

    def configure(self, vers=False):
        if not vers:
            for v in self.latest():
                print(self._pkg.configure.args)
                print(f'{self._cfg.dirs.temp}/{self._pkg.name}-{v}')
                obj = Configure(
                    self._pkg.configure.args,
                    f'{self._cfg.dirs.temp}/{self._pkg.name}-{v}'
                )
        
    def build(self, v=False):
        pass

    def install(self, v=False):
        pass

    def uri(self, vers=False):
        if vers and vers in self.versions:
            return self.uris[vers]
        return False
        
    def latest(self):
        retv = []
        for v in self._pkg.versions.activate:
            for tv in self.versions:
                if re.match(v, tv):
                    retv.append(tv)
                    break

        if len(retv) == 0:
            return False

        return retv
