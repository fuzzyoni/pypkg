
# Stdlib

import os
import copy
import tempfile

# 3rd party

import yaml
from edict import Edict


class Config(Edict):

    def __init__(self, cfn=False):
        if cfn and os.path.isfile(cfn):
            Edict.__init__(self, yaml.load(open(cfn).read()))
        elif not cfn:
            for fn in (f'{os.getenv("HOME")}/.pypkgrc', '/etc/pypkg.yml'):
                if os.path.isfile(fn):
                    Edict.__init__(self, yaml.load(open(fn).read()))
        else:
            raise(OSError(f'{cfn} does not exist.'))

        os_n = os.uname().sysname
        os_a = os.uname().machine
        os_t = f'{os_n}/{os_a}'
        
        # Set our PREFIX
        if 'dirs' in self.keys() and 'base' in self.dirs.keys():
            if self.dirs.base[0] == '~':
                t = f'{os.getenv("HOME")}/{self.dirs.base[2:]}'
                self.dirs.base = t

            if 'root' not in self.dirs.keys():
                self.dirs.root = f'{self.dirs.base}/{os_t}/root'
            else:
                t = f'{self.dirs.base}/{os_t}/{self.dirs.root}'
                self.dirs.root = t
                
            if 'library' not in self.dirs.keys():
                self.dirs.library = f'{self.dirs.base}/{os_t}/library'
            else:
                t = f'{self.dirs.base}/{os_t}/{self.dirs.library}'
                self.dirs.library = t

            if 'packages' not in self.dirs.keys():
                self.dirs.packages = f'{self.dirs.base}/packages'
            else:
                t = f'{self.dirs.base}/{self.dirs.packages}'
                self.dirs.packages = t
                
            # Setup our tempdir
            self.dirs['temp'] = tempfile.mkdtemp(prefix='pypkg.')

            # Make sure our dirs are present
            for key in self.dirs.keys():
                if not os.path.isdir(self.dirs[key]):
                    os.makedirs(self.dirs[key])

