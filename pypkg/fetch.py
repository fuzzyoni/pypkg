
# Stdlib

import os
import urllib.error
import urllib.request


def fetch(uri=False, odir=False):
    try:
        fn = os.path.basename(uri)
        fp = open(odir+'/'+fn, 'bw+')
        rq = urllib.request.Request(uri)

        cn = urllib.request.urlopen(rq)
        buff = cn.read(1048576)
        while buff:
            fp.write(buff.strip())
            buff = cn.read(1048576)
        fp.close()
        return True
    except urllib.error.HTTPError:
        return False
    except urllib.error.URLError:
        return False
    except:
        return 2
