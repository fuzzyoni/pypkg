
# Stdlib

import os
import subprocess


class Command(object):

    class Log(object):

        def __init__(self, *args, **kwargs):
            for t in ('tag', 'fp'):
                if t not in kwargs.keys():
                    raise(ValueError)
            self._tag = kwargs['tag']
            self._fp  = kwargs['fp']

        def write(self, data):
            self._fp.write('%s: %s\n' % (self._tag, data.strip()))
            self._fp.flush()
            
        def fileno(self):
            return self._fp.fileno()

        def close(self):
            return self._fp.close()


    def __init__(self, *args, **kwargs):
        for itm in ('tag', 'command'):
            if itm not in kwargs.keys():
                raise(ValueError)

        self._tag = kwargs['tag']
        cmd = kwargs['command']
        if type(cmd) == str:
            self._cmd = cmd
        elif type(cmd) in (list, tuple):
            self._cmd = ' '.join(cmd)
            
    def _which(self, query):
        for d in os.getenv('PATH').split(':'):
            if query in os.listdir(d):
                return d+'/'+query
        return False

    def run(self, chd=False):
        if chd and os.path.isdir(chd):
            os.chdir(chd)
        self._stdout = self.Log(tag='STDOUT',
                                fp=open('/tmp/pypkg-'+self._tag+'-stdout.log', 'w+'))
        self._proc = subprocess.Popen(
            self._cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            shell=True
        )
        buff = self._proc.stdout.readline()
        while buff:
            self._stdout.write(buff)
            buff = self._proc.stdout.readline()

