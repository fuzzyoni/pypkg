
# Stdlib

import os
import sys

# Internal

from .command import Command

class Build(Command):

    def __init__(self, ddir=False, cmd=False):
        if False in (cmd, ddir):
            raise(ValueError('Invalid values given to Build() object constructor.'))

        self.ddir = ddir

        command = ' '.join(cmd)
        Command.__init__(self, tag=f'build-{os.path.basename(ddir)}', command=command)
        self.run()



