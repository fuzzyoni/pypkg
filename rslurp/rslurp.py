
# Stdlib
import re
import sys

# 3rd party
from edict import Edict

class Rslurp(object):

    __callbacks = []

    def __init__(self, ifp=False):
        if ifp and 'readline' in dir(ifp):
            self.__ifp = ifp

    def set_input(self, fp=False):
        if ifp and 'readline' in dir(ifp):
            self.__ifp = fp
    
    def add_callback(self, pattern, callback):
        try:
            if type(pattern) not in (str, unicode):
                raise(ValueError, 'Invalid pattern provided.')
        except NameError:
            if type(pattern) != str:
                raise(ValueError, 'Invalid pattern provided.')

        if not callable(callback):
            raise(ValueError, 'Invalid trigger provided.')

        try:
            patt = re.compile(pattern)
        except re.sre_constants.error:
            return False
        
        payload = Edict({'pattern': patt, 'callback': callback})
        if payload not in self.__callbacks:
            self.__callbacks.append(payload)
            return True
        else:
            return None

    def process(self):
        buff = self.__ifp.readline()
        while buff:
            for entry in self.__callbacks:
                if entry.pattern.match(str(buff.strip())):
                    entry.callback(str(buff.strip()))
            buff = self.__ifp.readline()


if __name__ == '__main__':
    import yaml
    import urllib.request

    
    class TestObj(object):

        versions = []
    
        def __init__(self, yml):
            self.cfg = Edict(yaml.load(open(sys.argv[1]).read()))
            print(f'Name: {self.cfg.name}')
            print(f'Description: {self.cfg.description}')
            req = urllib.request.Request(self.cfg.url)
            self._slurp = Rslurp(urllib.request.urlopen(req))
            self._slurp.add_callback(self.cfg.pattern, self.callback)
            self._slurp.process()

        def callback(self, line):
            match = re.match(self.cfg.pattern, line)
            if match:
                print(match.group(1))
                print(match.group(2))
            if 1 == 2:
                uri = line.split('href="')[1].split('">')[0]
                print(uri)
                a = uri.split('/')[-1]
                print(a)
                b = a.split('.t')[0]
                print(b)
                version = b.split(f'{self.cfg.name[1:]}-')[1]
                print(version)
                print('')
                if version not in self.versions:
                    self.versions.append(version)

        def latest(self, patt):
            for v in self.versions:
                if re.match(patt, v):
                    return v
    
    from distutils.version import StrictVersion
    obj = TestObj('https://www.python.org/downloads/source/')
    obj.versions.sort(key=StrictVersion)
    obj.versions.reverse()
    for i in obj.cfg.install:
        print(f'https://www.python.org/ftp/python/{obj.latest(i)}/Python-{obj.latest(i)}.tgz')
