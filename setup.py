#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='edict',
    version='0.1.0',
    description='A slightly extended dictionary object.',
    author="Mike 'Fuzzy' Partin",
    author_email='fuzzy@fumanchu.org',
    url='https://git.thwap.org/fuzzy/edict',
    packages=['edict']
)

setup(
    name='rslurp',
    version='0.1.0',
    description='A simple regex parsing engine.',
    author="Mike 'Fuzzy' Partin",
    author_email='fuzzy@fumanchu.org',
    url='https://git.thwap.org/fuzzy/slurp',
    packages=['rslurp']
)

setup(
    name='pypkg',
    version='0.1.0',
    description='A personal package manager.',
    author="Mike 'Fuzzy' Partin",
    author_email='fuzzy@fumanchu.org',
    url='https://git.thwap.org/fuzzy/pypkg',
    packages=['pypkg']
)
